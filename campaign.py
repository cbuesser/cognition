#!/usr/bin/env python3

from candidate import Candidate
from pathlib import Path
import csv

with open("API_KEY","r") as api_file:
    api_key = api_file.read()
    api_key = api_key.rstrip()


responses = Candidate.getRepCandidates(api_key,2018)

filename = "Key.csv"
fieldnames = ["cand_id", "name" , "office", "committee_id", "state","district"]

people = []
key_file = Path(filename)

if key_file.is_file():
    with open(filename, "r") as key_csv:
        reader = csv.DictReader(key_csv, fieldnames=fieldnames)
        for row in reader:
            people.append(row)
else:
    with open(filename,"w") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()


def isInKeyFile(candidate_id, people):
    for person in people:
        if person["cand_id"] == candidate_id:
            return True
    return False

for response in responses:
    if isInKeyFile(response["candidate_id"], people):
        continue
    cand = Candidate(response)
    cand.pretty()
    cand.expenses(api_key)
    cand.writeCSVLine(filename,fieldnames)
    people.append(cand.__dict__)
    

