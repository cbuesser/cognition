from http import client
from urllib import parse
import json
import csv

MAX_PER_PAGE = 100

class Expenses:
    def __init__(self, candidate_id,cand_name, cid, state,api_key):
        self.cid = cid
        self.candidate_id = candidate_id
        self.cand_name = cand_name
        self.state = state
        self.client = client.HTTPSConnection('api.open.fec.gov')
        self.api_key = api_key
        self.url = '/v1/committee/' + cid + '/schedules/schedule_b/by_recipient/?'
        self.page = 1
        self.query = parse.urlencode({"api_key":self.api_key,"per_page":MAX_PER_PAGE,"page":self.page})

    def __del__(self):
        self.client.close()

    def setPage(self, page):
        self.page = page
        self.query = parse.urlencode({"api_key":self.api_key,"per_page":MAX_PER_PAGE,"page":self.page})

    def getResp(self):
        try:
            self.client.request('GET', self.url+self.query)
            resp = self.client.getresponse()
        except Exception as e:
            print(e.__doc__)
            print(e)
            sys.exit(1)

        return json.loads(resp.read().decode('utf-8'))


    def expenses(self):
        print("Generating Expeneses over 10k for: " + self.cid)
        result = self.getResp()

        pages = result['pagination']['pages']

        above10k = []

        while (self.page <= pages):
            result = self.getResp()

            page = result['pagination']['page']

            print("querying page " + str(page))

            for expense in result['results']:
                if expense['total'] > 10000:
                    above10k.append({"candidate_name":self.cand_name,
                        "candidate_id":self.candidate_id,
                        "state":self.state,
                        "name":expense['recipient_name'],
                        "total":expense['total'], 
                        "cycle": expense["cycle"]})
                if expense['memo_total'] > 10000:
                    above10k.append({"candidate_name":self.cand_name,
                        "candidate_id":self.candidate_id,
                        "state":self.state,
                        "name":expense['recipient_name'],
                        "total":expense['memo_total'], 
                        "cycle": expense["cycle"]})

            self.setPage(self.page + 1)

        return(above10k)

    def writeCSV(self):
        exp = self.expenses()
        with open(self.cid + ".csv", "w", newline='') as csvfile:
            fieldnames = ['candidate_id','candidate_name','state','name','total','cycle']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for val in exp:
                writer.writerow(val)









