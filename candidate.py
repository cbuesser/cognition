from urllib import parse
from expenses import Expenses

import http
import json
import csv

MAX_PER_PAGE = 100

def tryapi(client,url,query):

    try:
        client.request('GET', url+query)
        resp = client.getresponse()
    except Exception as e:
        print(e.__doc__)
        print(e)
        sys.exit(1)

    return json.loads(resp.read().decode('utf-8'))


class Candidate:
    def __init__(self, candidate):
        self.cand_id = candidate["candidate_id"]
        self.name = candidate["name"]
        self.office = candidate["office"]
        self.state = candidate["state"]
        self.district = candidate["district"]
        self.committee_id = None
        for committee in candidate["principal_committees"]:
            if committee["designation"] == "P":
                self.committee_id = committee["committee_id"]

    def expenses(self,api_key):
        if self.committee_id != None:
            exp = Expenses(self.cand_id, self.name, self.committee_id, self.state, api_key)
            exp.writeCSV()

    @staticmethod
    def getRepCandidates(api_key,year):
        client = http.client.HTTPSConnection('api.open.fec.gov')
        page = 1
        candidates = []
        url = '/v1/candidates/search/?'
        query = parse.urlencode({
            "api_key":api_key,
            "per_page":MAX_PER_PAGE,
            "page":page,
            "sort":"name",
            "party":"REP",
            "office":"H",
            "office":"S",
            "cycle": year,
            })

        result = tryapi(client,url,query)
        pages = result['pagination']['pages']

        while (page <= pages):
            result = tryapi(client,url,query)

            candidates = candidates + result["results"]

            page = page + 1
            query = parse.urlencode({
                "api_key":api_key,
                "per_page":MAX_PER_PAGE,
                "page":page,
                "sort":"name",
                "party":"REP",
                "office":"H",
                "office":"S",
                "cycle": year,
                })
        return candidates

    def writeCSVLine(self,filename,fieldnames):
        with open(filename, "a+") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writerow(self.__dict__)






    def pretty(self):
        print("Candidate: "+self.name+" State: "+self.state)
